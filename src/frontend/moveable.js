function createNewMoveableImage(target) {
    var moveable = new Moveable(document.body, {
        target: target,
            draggable: true,
            scalable: true,
            keepRatio: true,
            throttleScale: 0,
            renderDirections: ["nw","n","ne","w","e","sw","s","se"],
            edge: false,
            zoom: 1,
            origin: true,
            padding: {"left":0,"top":0,"right":0,"bottom":0},
        });
    
    console.log("moveable: ", moveable)
    moveable.on("drag", ({ target, transform }) => {
        target.style.transform = transform;
    });
    
    moveable.on("scale", ({ target, transform }) => {
        target.style.transform = transform;
    });

    return moveable;
}