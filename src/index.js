const path = require('path');
const express = require('express');
const bodyParser = require("body-parser");
const cors = require('cors');
const app = express();
const port = 3000


const defaultRoutes = require("./routes")();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const allowedOrigins = ['http://localhost:4200'];
app.use(cors({
    origin: function (origin, callback) {
        if (!origin) return callback(null, true);
        if (allowedOrigins.indexOf(origin) === -1) {
            const msg = 'The CORS policy for this site does not allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    },
    credentials: true,
}));

app.use("/api", defaultRoutes);
app.get('/sig', (req, res) => res.send('Signature box'));
console.log("dirname ", __dirname)
app.use(express.static("frontend"));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));